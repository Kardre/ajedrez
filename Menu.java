import java.util.Scanner;

public class Menu {

	Scanner scanner = new Scanner(System.in);	

	Empresa miEmpresa = new Empresa();

	public void showLogin() {
		System.out.println("Introduzca usuario");
		String usuario = scanner.nextLine();
		Empleado buscar = miEmpresa.buscarEmpleado(usuario);
		if (buscar != null) {
			System.out.println("Introduzca su contrase�a");
			String contrase�a = scanner.nextLine();
			if (contrase�a.equals(buscar.getPassword()) ) {
				showMenu();
			} else {
				System.out.println("Introduzca correctamente su contrase�a");
				showLogin();
			}

		}

	}


	public void showMenuEmpleado() {
		System.out.println("�Que quiere hacer?\n1.- A�adir producto.\n2.- Eliminar producto.\n3.- Lista de productos.\n4.- Editar precio.\n5.- Salir.");

		int opcion = new Util().validateRange(1, 5);
		
		if (opcion == 1) {
			a�adirPizza();
		} else if (opcion ==2) {
			eliminarPizza();
		}else if (opcion == 3) {
			imprimirPizzas();
		}else if(opcion == 4){

		} else if(opcion ==5) {
			
		}else{
			showMenuEmpleado();
		}
	}

	public void showMenu() {

		System.out.println("�Que quiere hacer?\n1.- A�adir empleado.\n2.- Eliminar empleado.\n3.- Lista de empleados.\n4.- Salir.");

		int opcion = new Util().validateRange(1, 4);
		if (opcion == 1) {
			a�adirPipol();
		} else if (opcion ==2) {
			eliminarPipol();
		}else if (opcion == 3) {
			imprimirPipol();
		}else if(opcion == 4){


		} else {
			System.out.println("Introduzca parametro correcto.");
			showMenu();
		}
	}

	public void a�adirPipol() {

		System.out.println("Introduzcca Username");
		String user = scanner.nextLine();
		System.out.println("Introduzca Password");
		String password = scanner.nextLine();
		System.out.println("Introduzca Nombre");
		String name = scanner.nextLine();
		Empleado empleado = new Empleado(user, password, name);
		miEmpresa.anadirEmpleado(empleado);
		showMenu();

	}
	public void imprimirPipol() {
		for ( int i = 0; i < miEmpresa.empleados.size(); i++) {
			System.out.println(miEmpresa.empleados.get(i));
		}
		showMenu();
	}
	public void eliminarPipol() {
		System.out.println("Introduzca el usuario a eliminar");
		String user = scanner.nextLine();
		miEmpresa.eliminarEmpleado(user);
		showMenu();
	}

	public void a�adirPizza() {
		System.out.println("�Que pizza quiere a�adir?");
		String nombre = scanner.nextLine();
		System.out.println("�Que precio tiene?");
		double precio = scanner.nextDouble();
		scanner.nextLine();
		Pizza pizza = new Pizza(nombre, precio);
		miEmpresa.a�adirPizza(pizza);
		showMenuEmpleado();
	}
	public void imprimirPizzas() {
		for ( int i = 0; i < miEmpresa.pizzas.size(); i++) {
			System.out.println(miEmpresa.pizzas.get(i));
		}
		showMenuEmpleado();
	}
	public void eliminarPizza() {
		System.out.println("�Que pizza quieres eliminar?");
		String nombre = scanner.nextLine();
		miEmpresa.eliminarPizza(nombre);
		showMenuEmpleado();
		
	}
	
}

