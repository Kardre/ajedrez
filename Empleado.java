
public class Empleado  {

	String user;
	String password;
	String name;

	
	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	Empleado(String user, String password, String name){
		this.user = user;
		this.password = password;
		this.name = name;

	}
	
	Empleado(String user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Empleado" +"  " + user + "  " + password +"  " + name;
	}
}
