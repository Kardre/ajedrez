import java.util.ArrayList;

public class Empresa {

	ArrayList<Empleado> empleados = new ArrayList<Empleado>();
	ArrayList<Pizza> pizzas = new ArrayList<Pizza>();

	Empresa(){
		empleados.add(new Empleado("pepe", "1234", "pepe"));
		empleados.add(new Empleado("juan", "4321", "juan"));
		pizzas.add( new Pizza ("pi�a", 12 ));
	}

	void a�adirPizza(Pizza pizza){
		pizzas.add(pizza);
	}

	void eliminarPizza1(String nombre) {
		for (int i = 0; i < pizzas.size(); i++) {
			pizzas.remove(nombre);
		}
	}

	void anadirEmpleado(Empleado empleado) {
		empleados.add(empleado);
	}
	void eliminarEmpleado(Empleado empleado) {
		for ( int i = 0; i <empleados.size(); i++) {
			empleados.remove(0);
		}
	}

	void a�adirEmpleado(String user, String password, String name) {

		Empleado empleado = new Empleado(user, password, name);
		anadirEmpleado(empleado);
	}
	void eliminarEmpleado(String busqueda) {

		Empleado empleadoEncontrado = buscarEmpleado(busqueda);

		if (empleadoEncontrado != null) {
			empleados.remove(empleadoEncontrado);
			System.out.println("Usuario " + empleadoEncontrado.getUser() + " eliminado" );
		}
	}
	void eliminarPizza(String busqueda) {

		Pizza pizzaEncontrada = buscarPizza(busqueda);

		if (pizzaEncontrada != null) {
			pizzas.remove(pizzaEncontrada);
			System.out.println("Pizza  " + pizzaEncontrada.getNombre() + " eliminada" );
		}
	}

	Pizza buscarPizza(String busqueda) {
		Pizza pizza = null;

		for (int i = 0; i < pizzas.size(); i++) {
			Pizza actualEnElArray= pizzas.get(i);	

			if (actualEnElArray.getNombre().equals(busqueda)) {
				pizza = actualEnElArray;
			} 
		}
		if (pizza == null) {
			System.out.println("Pizza no encontrada");
		}
		return pizza;
	}


	Empleado buscarEmpleado(String busqueda) {
		Empleado empleado = null;

		for (int i = 0; i < empleados.size(); i++) {
			Empleado actualEnElArray= empleados.get(i);	

			if (actualEnElArray.getUser().equals(busqueda)) {
				empleado = actualEnElArray;
			} 
		}
		if (empleado == null) {
			System.out.println("Usuario no encontrado");
		}
		return empleado;
	}
}
