
import java.util.Scanner;

import sun.jvm.hotspot.ui.action.ShowAction;

public class Util {

	public int validateRange(int min, int max) {
		
		boolean isValidOpcion = false;
		Scanner scanner = new Scanner(System.in);
		int result = -1;
		while (!isValidOpcion) {
			int opcion = scanner.nextInt();

			scanner.nextLine();
			if ( opcion < min || opcion > max) {
				System.out.println("Introduzca una opcion " + min + " y " + max + ".");
			}else {
				result = opcion;
				isValidOpcion = true;
			}
		}
		return result;
	}
}
