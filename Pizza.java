
public class Pizza {

	public Pizza(String nombre, double precio) {
		this.nombre = nombre;
		this.precio = precio;
	}
	public Pizza() {
		
	}
	double precio;
	String nombre;
	
	@Override
	public String toString() {
		return "La pizza " + nombre + " cuesta " + precio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}
